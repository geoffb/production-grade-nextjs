import React, { FC } from 'react'
// import hydrate from 'next-mdx-remote/hydrate'
import { majorScale, Pane, Heading, Spinner } from 'evergreen-ui'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { Post } from '../../types'
import Container from '../../components/container'
import HomeNav from '../../components/homeNav'
import path from 'path'
import fs from 'fs'
import matter from 'gray-matter'
import { posts } from '../../content'
import { getPost } from '../../utils/posts'
import { renderToString } from 'react-dom/server'

const BlogPost: FC<Post> = ({ source, frontMatter }) => {
  // const content = hydrate(source)
  const router = useRouter()

  if (router.isFallback) {
    return (
      <Pane width="100%" height="100%">
        <Spinner size={48} />
      </Pane>
    )
  }
  return (
    <Pane>
      <Head>
        <title>{`Known Blog | ${frontMatter.title}`}</title>
        <meta name="description" content={frontMatter.summary} />
      </Head>
      <header>
        <HomeNav />
      </header>
      <main>
        <Container>
          <Heading fontSize="clamp(2rem, 8vw, 6rem)" lineHeight="clamp(2rem, 8vw, 6rem)" marginY={majorScale(3)}>
            {frontMatter.title}
          </Heading>
          <pre>{source}</pre>
          {/*<Pane>{source}</Pane>*/}
        </Container>
      </main>
    </Pane>
  )
}

BlogPost.defaultProps = {
  source: '',
  frontMatter: { title: 'default title', summary: 'summary', publishedOn: '' },
}

/**
 * Need to get the paths here
 * then the correct post for the matching path
 * Posts can come from the fs or our CMS
 */
export default BlogPost

export function getStaticPaths() {
  const postsPath = path.join(process.cwd(), 'posts')
  const filenames = fs.readdirSync(postsPath)

  const posts = filenames.map((filename) => {
    const file = fs.readFileSync(`posts/${filename}`, 'utf8')
    const { data } = matter(file)
    return data
  })

  return {
    paths: posts.map((post) => ({ params: { slug: post.slug } })),
    fallback: true,
  }
}

export async function getStaticProps({ params }) {
  const post = getPost({ slug: params.slug })

  if (!post) {
    return {
      props: {
        frontMatter: { title: 'Post not found' },
      },
    }
  }

  const { content, data } = matter(post)
  const mdxSource = await renderToString(content, { scope: data })

  return {
    props: { source: mdxSource, frontMatter: data },
    revalidate: 30,
  }
}

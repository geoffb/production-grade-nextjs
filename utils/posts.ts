import path from 'path'
import fs from 'fs'
import { posts } from '../content'
import matter from 'gray-matter'

export const getPost = ({ slug }: { slug: string }) => {
  try {
    const postPath = path.join(process.cwd(), 'posts', `${slug}.mdx`)
    return fs.readFileSync(postPath, 'utf8')
  } catch {
    console.log('Not found')

    const cmsPosts = posts.published.map((post) => {
      return matter(post)
    })

    return cmsPosts.find((post) => post.data.slug === slug)
  }
}
